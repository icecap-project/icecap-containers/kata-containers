// Copyright (c) 2016 Intel Corporation
//
// SPDX-License-Identifier: Apache-2.0
//

package virtcontainers

import (
	"context"
	"errors"
	"os"
	"os/exec"
	log "github.com/sirupsen/logrus"
	persistapi "github.com/kata-containers/kata-containers/src/runtime/virtcontainers/persist/api"
	"github.com/kata-containers/kata-containers/src/runtime/virtcontainers/types"
)

const (
	icecapSocket		= "icecap.sock"
	defaultIcecapPath	= "/icecap-host"
	specPath			= "/spec.bin"
)

//var MockHybridVSockPath = "/tmp/kata-mock-hybrid-vsock.socket"

//hypervisor interface for IceCap 
type icecap struct {
	id        string	//realm id
	config    HypervisorConfig
	ctx       context.Context
	realmId	string
	spec	string //should be blob of data 
	icecapHost	string	//icecap-host proocess path
	//strage
	//network
}

func (ic *icecap) capabilities(ctx context.Context) types.Capabilities {
	caps := types.Capabilities{}
	caps.SetFsSharingSupport()
	return caps
}

func (ic *icecap) hypervisorConfig() HypervisorConfig {
	return ic.config
}

// Logger returns icecap log messages
func (clh *icecap) Logger() *log.Entry {
	return virtLog.WithField("subsystem", "icecap")
}

// just setup incecap sandbox structure. it will be created here but started in startSandbox()
func (ic *icecap) createSandbox(ctx context.Context, id string, networkNS NetworkNamespace, hypervisorConfig *HypervisorConfig) error {
	ic.ctx = ctx
	
	/*err := hypervisorConfig.valid()
	if err != nil {
		return err
	}
	*/
	ic.Logger().WithField("function", "createSandbox").Info("creating Sandbox")
	ic.id = id
	ic.realmId = "0"
	ic.config = *hypervisorConfig
	ic.icecapHost = defaultIcecapPath
	ic.spec = specPath
	//state = not ready at this point
	// OCI container image path: check path?
	// socket addr
	// if it already exists: icecap-host destroy 0
	args := []string{
		"create",
		ic.realmId,
		ic.spec,
	}
	cmd := exec.CommandContext(ctx, ic.icecapHost, args...)
	
	if err := cmd.Start(); err != nil {
		ic.Logger().WithField("function", "createSandbox").Info("creating Sandbox failed")
	}
	ic.Logger().WithField("function", "createSandbox").Info("Sandbox created")

	//temp: start 
	arg := []string{
		"run",
		ic.realmId,
		"0",
	}
	ic.Logger().WithField("function", "startSandbox").Info("starting Sandbox")
	cmdd := exec.CommandContext(ctx, ic.icecapHost, arg...)
	
	if err := cmdd.Start(); err != nil {
		//log.Fatal(err)
		ic.Logger().WithField("function", "startSandbox").Info("starting Sandbox failed")
	}
	ic.Logger().WithField("function", "startSandbox").Info("Sandbox started")
	return nil
}

//
func (ic *icecap) startSandbox(ctx context.Context, timeout int) error {
	args := []string{
		"run",
		ic.realmId,
		"0",
	}
	ic.Logger().WithField("function", "startSandbox").Info("starting Sandbox")
	cmd := exec.CommandContext(ctx, ic.icecapHost, args...)
	
	if err := cmd.Start(); err != nil {
		//log.Fatal(err)
		ic.Logger().WithField("function", "startSandbox").Info("starting Sandbox failed")
	}
	ic.Logger().WithField("function", "startSandbox").Info("Sandbox started")
	return nil
}

func (ic *icecap) stopSandbox(ctx context.Context, waitOnly bool) error {
	return nil
}

func (ic *icecap) pauseSandbox(ctx context.Context) error {
	return nil
}

func (ic *icecap) resumeSandbox(ctx context.Context) error {
	return nil
}

func (ic *icecap) saveSandbox() error {
	return nil
}

func (ic *icecap) addDevice(ctx context.Context, devInfo interface{}, devType deviceType) error {
	return nil
}

func (ic *icecap) hotplugAddDevice(ctx context.Context, devInfo interface{}, devType deviceType) (interface{}, error) {
	switch devType {
	case cpuDev:
		return devInfo.(uint32), nil
	case memoryDev:
		memdev := devInfo.(*memoryDevice)
		return memdev.sizeMB, nil
	}
	return nil, nil
}

func (ic *icecap) hotplugRemoveDevice(ctx context.Context, devInfo interface{}, devType deviceType) (interface{}, error) {
	switch devType {
	case cpuDev:
		return devInfo.(uint32), nil
	case memoryDev:
		return 0, nil
	}
	return nil, nil
}

func (ic *icecap) getSandboxConsole(ctx context.Context, sandboxID string) (string, string, error) {
	return "", "", nil
}

func (ic *icecap) resizeMemory(ctx context.Context, memMB uint32, memorySectionSizeMB uint32, probe bool) (uint32, memoryDevice, error) {
	return 0, memoryDevice{}, nil
}
func (ic *icecap) resizeVCPUs(ctx context.Context, cpus uint32) (uint32, uint32, error) {
	return 0, 0, nil
}

func (ic *icecap) disconnect(ctx context.Context) {
}

func (ic *icecap) getThreadIDs(ctx context.Context) (vcpuThreadIDs, error) {
	vcpus := map[int]int{0: os.Getpid()}
	return vcpuThreadIDs{vcpus}, nil
}

func (ic *icecap) cleanup(ctx context.Context) error {
	return nil
}

func (ic *icecap) getPids() []int {
	return []int{1}
	//return []int{m.mockPid}
}

func (ic *icecap) getVirtioFsPid() *int {
	return nil
}

func (ic *icecap) fromGrpc(ctx context.Context, hypervisorConfig *HypervisorConfig, j []byte) error {
	return errors.New("IceCap is not supported by VM cache")
}

func (ic *icecap) toGrpc(ctx context.Context) ([]byte, error) {
	return nil, errors.New("IceCap is not supported by VM cache")
}

func (ic *icecap) save() (s persistapi.HypervisorState) {
	return
}

func (ic *icecap) load(s persistapi.HypervisorState) {}

func (ic *icecap) check() error {
	return nil
}

func (ic *icecap) generateSocket(id string) (interface{}, error) {
	return types.MockHybridVSock{
		UdsPath: MockHybridVSockPath,
	}, nil
}

func (ic *icecap) isRateLimiterBuiltin() bool {
	return false
}

func (ic *icecap) setSandbox(sandbox *Sandbox) {
}
